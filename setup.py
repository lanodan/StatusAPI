# -*- encoding: utf-8 -*-
from distutils.core import setup
from StatusAPI import __version__
import io


def read(*filenames, **kwargs):
    encoding = kwargs.get('encoding', 'utf-8')
    sep = kwargs.get('sep', '\n')
    buf = []
    for filename in filenames:
        with io.open(filename, encoding=encoding) as f:
            buf.append(f.read())
    return sep.join(buf)

setup(
    name='StatusAPI',
    version=__version__,
    author='Jonas Geduldig, Haelwenn Monnier',
    author_email='lanodan.delta@free.fr',
    packages=['StatusAPI'],
    package_data={'': ['config.py']},
    url='https://gitlab.com/lanodan/StatusAPI',
    download_url='https://gitlab.com/lanodan/StatusAPI/repository/archive.tar.gz',
    license='MIT',
    keywords='twitter, GnuSocial, REST API',
    description='Minimal wrapper for Twitter’s REST and Streaming APIs',
#    install_requires=['requests', 'requests_oauthlib']
	requires=['requests', 'requests_oauthlib']
)
