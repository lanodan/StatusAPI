"""
	Constants For All Twitter API Endpoints
	-----------------------------------
	
	Version 1.1, Streaming API and REST API.
	
	URLs for each endpoint are composed of the following pieces:
		BASEURL/{resource}?{parameters}

	example of API_URL:
	• twitter: https://api.twitter.com/1.1/
	• loadaverage: https://loadaverage.org/

	BASEURL has to be defined by the client, not the library
"""


__author__ = "Haelwenn Monnier"
__date__ = "2015-04-23"
__license__ = "MIT"


USER_AGENT = 'python-TwitterAPI'

STREAMING_TIMEOUT = 90
REST_TIMEOUT = 5

ENDPOINTS = {
# https://stream.twitter.com/1.1/
	# resource:                                (method, mime-type)

	'statuses/filter':                         ('POST'),
	'statuses/firehose':                       ('GET'),
	'statuses/sample':                         ('GET'),
	'site':                                    ('GET'),
	'user':                                    ('GET'),

# https://media.twitter.com/1.1/
	'media/upload':                            ('POST'),

# https://api.twitter.com/1.1/
	'account/remove_profile_banner':           ('POST'),
	'account/settings':                        ('GET'),
	'account/update_delivery_device':          ('POST'),
	'account/update_profile':                  ('POST'),
	'account/update_profile_background_image': ('POST'),
	'account/update_profile_banner':           ('POST'),
	'account/update_profile_colors':           ('POST'),
	'account/update_profile_image':            ('POST'),
	'account/verify_credentials':              ('GET'),

	'application/rate_limit_status':           ('GET'),
	
	'blocks/create':                           ('POST'),
	'blocks/destroy':                          ('POST'),
	'blocks/ids':                              ('GET'),
	'blocks/list':                             ('GET'),

	'direct_messages':                         ('GET'),
	'direct_messages/destroy':                 ('POST'),
	'direct_messages/new':                     ('POST'),
	'direct_messages/sent':                    ('GET'),
	'direct_messages/show':                    ('GET'),

	'favorites/create':                        ('POST'),
	'favorites/destroy':                       ('POST'),
	'favorites/list':                          ('GET'),

	'followers/ids':                           ('GET'),
	'followers/list':                          ('GET'),

	'friends/ids':                             ('GET'),
	'friends/list':                            ('GET'),

	'friendships/create':                      ('POST'),
	'friendships/destroy':                     ('POST'),
	'friendships/incoming':                    ('GET'),
	'friendships/lookup':                      ('GET'),
	'friendships/no_retweets/ids':             ('GET'),
	'friendships/outgoing':                    ('GET'),
	'friendships/show':                        ('GET'),
	'friendships/update':                      ('POST'),

	'lists/create':                            ('POST'),
	'lists/destroy':                           ('POST'),
	'lists/list':                              ('GET'),
	'lists/members':                           ('GET'),
	'lists/members/create':                    ('POST'),
	'lists/members/create_all':                ('POST'),
	'lists/members/destroy':                   ('POST'),
	'lists/members/destroy_all':               ('POST'),
	'lists/members/show':                      ('GET'),
	'lists/memberships':                       ('GET'),
	'lists/ownerships':                        ('GET'),
	'lists/show':                              ('GET'),
	'lists/statuses':                          ('GET'),
	'lists/subscribers':                       ('GET'),
	'lists/subscribers/create':                ('POST'),
	'lists/subscribers/destroy':               ('POST'),
	'lists/subscribers/show':                  ('GET'),
	'lists/subscriptions':                     ('GET'),
	'lists/update':                            ('POST'),

	'mutes/users/create':                      ('POST'),
	'mutes/users/destroy':                     ('POST'),
	'mutes/users/ids':                         ('GET'),
	'mutes/users/list':                        ('GET'),

	'geo/id/:PARAM':                           ('GET'), # PLACE_ID
	'geo/place':                               ('POST'),
	'geo/reverse_geocode':                     ('GET'),
	'geo/search':                              ('GET'),
	'geo/similar_places':                      ('GET'),

	'help/configuration':                      ('GET'),
	'help/languages':                          ('GET'),
	'help/privacy':                            ('GET'),
	'help/tos':                                ('GET'),

	'saved_searches/create':                   ('POST'),
	'saved_searches/destroy/:PARAM':           ('POST'), # ID
	'saved_searches/list':                     ('GET'),
	'saved_searches/show/:PARAM':              ('GET'), # ID

	'search/tweets':                           ('GET'),

	'statuses/destroy/:PARAM':                 ('POST'), # ID
	'statuses/home_timeline':                  ('GET'),
	'statuses/lookup':                         ('GET'),
	'statuses/mentions_timeline':              ('GET'),
	'statuses/oembed':                         ('GET'),
	'statuses/retweet/:PARAM':                 ('POST'), # ID
	'statuses/retweeters/ids':                 ('GET'),
	'statuses/retweets/:PARAM':                ('GET'), # ID
	'statuses/retweets_of_me':                 ('GET'),
	'statuses/show/:PARAM':                    ('GET'), # ID
	'statuses/user_timeline':                  ('GET'),
	'statuses/update':                         ('POST'), # deprecated
	'statuses/update_with_media':              ('POST'),

	'trends/available':                        ('GET'),
	'trends/closest':                          ('GET'),
	'trends/place':                            ('GET'),

	'users/contributees':                      ('GET'),
	'users/contributors':                      ('GET'),
	'users/lookup':                            ('GET'),
	'users/profile_banner':                    ('GET'),
	'users/report_spam':                       ('POST'),
	'users/search':                            ('GET'),
	'users/show':                              ('GET'),
	'users/suggestions':                       ('GET'),
	'users/suggestions/:PARAM':                ('GET'), # SLUG
	'users/suggestions/:PARAM/members':        ('GET'),  # SLUG

# Here are StatusNet specific ressources
	'statusnet/groups/timeline':               ('GET'),
	'statusnet/groups/show':                   ('GET'),
	'statusnet/groups/create':                 ('POST'),
	'statusnet/groups/join':                   ('POST'),
	'statusnet/groups/leave':                  ('POST'),
	'statusnet/groups/list':                   ('GET'),
	'statusnet/groups/list_all':               ('GET'),
	'statusnet/groups/membership':             ('GET'),
	'statusnet/groups/is_member':              ('GET'),

	'statusnet/tags/timeline':                 ('GET'),

	'statusnet/media/upload':                  ('POST', 'multipart/form-data') # Similar to twitpic
}
