__title__ = 'StatusAPI'
__version__ = '0.0.1'
__author__ = 'Jonas Geduldig'
__license__ = 'MIT'
__copyright__ = 'Copyright 2013 Jonas Geduldig'


import logging


# No logging unless the client provides a handler
logging.getLogger(__name__).addHandler(logging.NullHandler())


try:
    from .StatusAPI import StatusAPI, StatusResponse, RestIterator, StreamingIterator
    from .StatusError import StatusConnectionError, StatusRequestError
    from .StatusOAuth import StatusOAuth
    from .StatusRestPager import StatusRestPager
except:
    pass


__all__ = [
    'StatusAPI',
    'StatusConnectionError',
    'StatusRequestError',
    'StatusOAuth',
    'StatusRestPager'
]
